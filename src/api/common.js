import request from "@/utils/request"

// 获取区块概况
export function getBlocksOverviewData() {
    return request.get({ url: `/overview/block` }, { isAuth: false })
}

// 获取区块列表
export function getBlocksList(data) {
    return request.get({ url: `/blocks/${data.p}/${data.ps}` }, { isAuth: false })
}

// 获取交易概况
export function getTransactionsOverviewData(data) {
    return request.get({ url: `/overview/transaction` }, { isAuth: false })
}

// 获取交易列表
export function getTransactionsList(data) {
    return request.get({ url: `/transactions/${data.p}/${data.ps}` }, { isAuth: false })
}

// 获取交易详情
export function getTransactionByTx(data) {
    return request.get({ url: `/transaction/${data.tx}` }, { isAuth: false })
}

// 获取首页概况
export function getOverviewData(data) {
    return request.get({ url: `/overview` }, { isAuth: false })
}

// 根据高度获取区块详情
export function getBlockByHeight(data) {
    return request.get({ url: `/block/${data.height}` }, { isAuth: false })
}

// 根据地址获取交易记录
export function getAddressTransactionsList(data) {
    return request.get({ url: `/transactions/address/${data.address}/${data.p}/${data.ps}` }, { isAuth: false })
}

// 获取账户排行列表
export function getAccountsList(data) {
    return request.get({ url: `/accounts/${data.p}/${data.ps}` }, { isAuth: false })
}
