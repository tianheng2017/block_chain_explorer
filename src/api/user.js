import request from "@/utils/request"

export function getUserInfo(header) {
    return request.get({ url: "/user/info", header }, { isAuth: true })
}