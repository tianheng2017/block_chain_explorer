import { createSSRApp } from "vue"
import App from "./App.vue"
import * as Pinia from "pinia"
import piniaPluginPersistedstate from "pinia-plugin-persistedstate"
import uviewPlus from "@/uni_modules/uview-plus"
import { createI18n } from "vue-i18n"
import en from "@/i18n/en.json"
import zhHans from "@/i18n/zh-Hans.json"
import "@/style/index.css"
import "@/static/font-awesome.min.css"

const i18nConfig = {
    legacy: false,
    locale: uni.getLocale(),
    messages: {
        en,
        "zh-Hans": zhHans
    }
}

export function createApp() {
    const pinia = Pinia.createPinia().use(piniaPluginPersistedstate)
    const i18n = createI18n(i18nConfig)
    const app = createSSRApp(App)
    app.use(pinia).use(uviewPlus).use(i18n)
    return {
        app,
        Pinia
    }
}
