import { getToken } from "@/utils/auth"
import { routes } from "./routes"
import { navigateTo } from "@/utils/tools"

const list = ["navigateTo", "redirectTo", "reLaunch", "switchTab"]
list.forEach((item) => {
    uni.addInterceptor(item, {
        invoke(e) {
            const url = e.url.split("?")[0]
            const currentRoute = routes.find((item) => {
                return url === item.path
            })

            if (currentRoute?.auth && !getToken()) {
                navigateTo("/pages/index/index")
                return false
            }
            return e
        },
        fail(err) {
            console.log(err)
        }
    })
})
