import { defineStore } from "pinia"
import { reactive } from "vue"

const useAppStore = defineStore(
    "app",
    () => {
        const cacheData = reactive({
            price: {
                nowPrice: 0,
                oldPrice: 0,
                trendDirection: 0,
                trendDirectionSymbol: "",
                risePercentage: 0,
                risePercentageColor: "#5A6773"
            },
            cap: {
                nowCap: 0,
                oldCap: 0,
                trendDirection: 0,
                trendDirectionSymbol: "",
                risePercentage: 0,
                risePercentageColor: "#5A6773"
            },
            fullCap: {
                nowFullCap: 0,
                oldFullCap: 0,
                trendDirection: 0,
                trendDirectionSymbol: "",
                risePercentage: 0,
                risePercentageColor: "#5A6773"
            }
        })

        const updatePrice = (newPrice) => {
            if (newPrice > cacheData.price.nowPrice) {
                cacheData.price.trendDirection = 1
                cacheData.price.trendDirectionSymbol = "↑"
                cacheData.price.risePercentageColor = "#FF4D4F"
            } else if (newPrice < cacheData.price.nowPrice) {
                cacheData.price.trendDirection = -1
                cacheData.price.trendDirectionSymbol = "↓"
                cacheData.price.risePercentageColor = "#52C41A"
            } else {
                cacheData.price.trendDirection = 0
                cacheData.price.trendDirectionSymbol = ""
                cacheData.price.risePercentageColor = "#5A6773"
            }
            if (cacheData.price.nowPrice == 0) {
                cacheData.price.risePercentage = 0
            } else {
                cacheData.price.risePercentage = Math.abs(
                    ((newPrice - cacheData.price.nowPrice) / cacheData.price.nowPrice) * 100
                ).toFixed(4)
            }
            cacheData.price.oldPrice = cacheData.price.nowPrice
            cacheData.price.nowPrice = newPrice
        }

        const updateCap = (newCap) => {
            if (newCap > cacheData.cap.nowCap) {
                cacheData.cap.trendDirection = 1
                cacheData.cap.trendDirectionSymbol = "↑"
                cacheData.cap.risePercentageColor = "#FF4D4F"
            } else if (newCap < cacheData.cap.nowCap) {
                cacheData.cap.trendDirection = -1
                cacheData.cap.trendDirectionSymbol = "↓"
                cacheData.cap.risePercentageColor = "#52C41A"
            } else {
                cacheData.cap.trendDirection = 0
                cacheData.cap.trendDirectionSymbol = ""
                cacheData.cap.risePercentageColor = "#5A6773"
            }
            if (cacheData.cap.nowCap == 0) {
                cacheData.cap.risePercentage = 0
            } else {
                cacheData.cap.risePercentage = Math.abs(
                    ((newCap - cacheData.cap.nowCap) / cacheData.cap.nowCap) * 100
                ).toFixed(4)
            }
            cacheData.cap.oldCap = cacheData.cap.nowCap
            cacheData.cap.nowCap = newCap
        }

        const updateFullCap = (newCap) => {
            if (newCap > cacheData.fullCap.nowFullCap) {
                cacheData.fullCap.trendDirection = 1
                cacheData.fullCap.trendDirectionSymbol = "↑"
                cacheData.fullCap.risePercentageColor = "#FF4D4F"
            } else if (newCap < cacheData.fullCap.nowFullCap) {
                cacheData.fullCap.trendDirection = -1
                cacheData.fullCap.trendDirectionSymbol = "↓"
                cacheData.fullCap.risePercentageColor = "#52C41A"
            } else {
                cacheData.fullCap.trendDirection = 0
                cacheData.fullCap.trendDirectionSymbol = ""
                cacheData.fullCap.risePercentageColor = "#5A6773"
            }
            if (cacheData.fullCap.nowFullCap == 0) {
                cacheData.fullCap.risePercentage = 0
            } else {
                cacheData.fullCap.risePercentage = Math.abs(
                    ((newCap - cacheData.fullCap.nowFullCap) / cacheData.fullCap.nowFullCap) * 100
                ).toFixed(4)
            }
            cacheData.fullCap.oldCap = cacheData.fullCap.nowFullCap
            cacheData.fullCap.nowFullCap = newCap
        }

        return {
            cacheData,
            updatePrice,
            updateCap,
            updateFullCap
        }
    },
    {
        persist: {
            paths: ["cacheData"]
        }
    }
)

export default useAppStore
