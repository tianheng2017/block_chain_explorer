import cache from "./cache"

export function getToken() {
    return cache.get("token")
}
