export const isWeixinClient = () => {
    // #ifdef H5
    return /MicroMessenger/i.test(navigator.userAgent)
    // #endif
}

export function isAndroid() {
    const u = navigator.userAgent
    return u.indexOf("Android") > -1 || u.indexOf("Adr") > -1
}

export const getClient = () => {
    return handleClientEvent({
        MP_WEIXIN: () => 1,
        OA_WEIXIN: () => 2,
        H5: () => 3,
        IOS: () => 5,
        ANDROID: () => 6,
        OTHER: () => null
    })
}

export const handleClientEvent = ({ MP_WEIXIN, OA_WEIXIN, H5, IOS, ANDROID, OTHER }) => {
    // #ifdef MP-WEIXIN
    return MP_WEIXIN()
    // #endif

    // #ifdef H5
    return isWeixinClient() ? OA_WEIXIN() : H5()
    // #endif

    // #ifdef APP-PLUS
    const system = uni.getSystemInfoSync()
    if (system.platform == "ios") {
        return IOS()
    } else {
        return ANDROID()
    }
    // #endif
    return OTHER()
}

export const client = getClient()
