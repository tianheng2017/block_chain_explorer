import { client } from "@/utils/client"
import cache from "./cache"

// 是否是APP环境
export const isApp = (() => {
    if (client == 5 || client == 6) {
        return true
    }
    return false
})()

// 是否是微信环境
export const isWeixin = (() => {
    if (client == 1) {
        return true
    }
    return false
})()

// 是否是H5环境
export const isH5 = (() => {
    if (client == 2 || client == 3) {
        return true
    }
    return false
})()

// js打开URL
export const openUrl = (url) => {
    // #ifdef APP-PLUS
    plus.runtime.openURL(url)
    // #endif

    // #ifdef H5
    window.open(url)
    // #endif

    // #ifdef MP-WEIXIN
    uni.setClipboardData({
        data: url,
        success: () => {}
    })
    uni.showModal({
        content: `小程序不支持访问外部链接，已自动复制网址，请在手机浏览器粘贴打开`,
        showCancel: false
    })
    // #endif
}

export const navigateTo = (link, navigateType = "") => {
    if (navigateType == "reLaunch") {
        return uni.reLaunch({
            url: link,
            animationType: "none"
        })
    }
    if (navigateType == "navigateTo") {
        return uni.navigateTo({
            url: link,
            animationType: "none"
        })
    }

    // #ifdef MP-WEIXIN
    return uni.reLaunch({
        url: link,
        animationType: "none"
    })
    // #endif

    // #ifndef MP-WEIXIN
    return uni.navigateTo({
        url: link,
        animationType: "none"
    })
    // #endif
}

export const crop = (str, number = 7) => {
    return parseInt(str?.length) > number ? str.substring(0, number) + "..." : str
}

export const pagingStyle = (number = 46) => {
    return {
        maxWidth: "576px",
        overflowX: "auto",
        margin: "0 auto",
        top: uni.$u.sys().statusBarHeight + number + "px"
    }
}

export const timeFormat = (dateTime, fmt = "yyyy-mm-dd hh:MM:ss") => {
    if (!dateTime) return ""
    if (dateTime.toString().length == 10) dateTime *= 1000
    let date = new Date(dateTime)
    let ret
    let opt = {
        "y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "h+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "s+": date.getSeconds().toString()
    }
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt)
        if (ret) {
            fmt = fmt.replace(ret[1], ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, "0"))
        }
    }
    return fmt
}

export const getLocalTime = () => {
    const now = new Date()
    const year = now.getFullYear()
    const month = now.getMonth() + 1
    const date = now.getDate()
    const hours = now.getHours()
    const minutes = now.getMinutes()
    const seconds = now.getSeconds()
    const formattedDateTime =
        year +
        "-" +
        (month < 10 ? "0" : "") +
        month +
        "-" +
        (date < 10 ? "0" : "") +
        date +
        " " +
        (hours < 10 ? "0" : "") +
        hours +
        ":" +
        (minutes < 10 ? "0" : "") +
        minutes +
        ":" +
        (seconds < 10 ? "0" : "") +
        seconds
    return formattedDateTime
}

// timeAgo 时间计算
export const timeAgo = (timestamp, seconds = 0) => {
    if (!timestamp) return ""

    let now = new Date()
    if (seconds > 0) {
        now = seconds
    } else if (cache.get("serverTime") > 0) {
        now = cache.get("serverTime")
    }

    // 服务器时间还是有误差，增加5秒避免显示负数
    seconds = Math.floor((new Date() - timestamp * 1000) / 1000) + 5
    let interval = Math.floor(seconds / 31536000)

    if (interval > 1) {
        return interval + (uni.getLocale() == "en" ? " years ago" : " 年前")
    }
    interval = Math.floor(seconds / 2592000)
    if (interval > 1) {
        return interval + (uni.getLocale() == "en" ? " months ago" : " 月前")
    }
    interval = Math.floor(seconds / 86400)
    if (interval > 1) {
        return interval + (uni.getLocale() == "en" ? " days ago" : " 天前")
    }
    interval = Math.floor(seconds / 3600)
    if (interval > 1) {
        return interval + (uni.getLocale() == "en" ? " hours ago" : " 小时前")
    }
    interval = Math.floor(seconds / 60)
    if (interval > 1) {
        return interval + (uni.getLocale() == "en" ? " minutes ago" : " 分钟前")
    }
    return Math.floor(seconds) + (uni.getLocale() == "en" ? " seconds ago" : " 秒前")
}

// 处理图表函数
export const chartHandle = (data) => {
    const temp = {
        categories: [],
        series: []
    }
    if (data.length == 0) return temp

    const seriesData = []
    data.map((item) => {
        temp.categories.push(item.date.substring(8, 10))
        seriesData.push(item.txn)
    })

    temp.series.push({
        name: uni.getLocale() == "en" ? " Daily trading volume" : " 日交易量",
        data: seriesData.reverse()
    })
    temp.categories.reverse()

    return temp
}
