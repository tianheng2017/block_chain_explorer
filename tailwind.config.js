/** @type {import('tailwindcss').Config} */
const plugin = require("tailwindcss/plugin")
module.exports = {
    content: ["./index.html", "./src/**/*.{html,js,ts,jsx,tsx,vue}"],
    theme: {
        colors: {
            white: "#ffffff",
            black: "#000000",
            main: "#333333",
            content: "#666666",
            muted: "#999999",
            light: "#e5e5e5",
            primary: {
                DEFAULT: "#4173ff"
            },
            success: "#5ac725",
            warning: "#f9ae3d",
            error: "#f56c6c",
            info: "#909399",
            page: "#f6f6f6",
            title2: "#a9a9a9",
            red: "#ff0000",
            dButton: "#07c160"
        },
        fontSize: {
            xs: "12px",
            sm: "13px",
            base: "14px",
            lg: "15px",
            xl: "16px",
            "2xl": "18px",
            "3xl": "19px",
            "4xl": "20px",
            "5xl": "22px"
        }
    },
    plugins: [
        require("tailwindcss"),
        require("autoprefixer"),
        plugin(function ({ addUtilities }) {
            const newUtilities = {}
            addUtilities(newUtilities, ["responsive"])
        })
    ],
    corePlugins: {
        preflight: false
    }
}
